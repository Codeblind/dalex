<?php
/**
 * Helpers for creating Drush shell-aliases.
 */
namespace Dalex;

/**
 * Class Dalex
 *
 * This class assists with creating common Drush shell-aliases commands.
 */
class Dalex {

  /**
   * Dalex constructor defines some constants used by our class.
   *
   */
  public function __construct() {
    define('DALEX_SQL_DROP', 'sql-drop');
    define('DALEX_SQL_SYNC', 'sql-sync');
    define('DALEX_RSYNC_FILES', 'rsync-files');
    define('DALEX_RSYNC_PRIVATE', 'rsync-private');
    define('DALEX_FRA', 'fra');
    define('DALEX_FRA_FORCE', 'fra-force');
    define('DALEX_UPDB', 'updb');
    define('DALEX_CC_ALL', 'cc-all');
  }


  /**
   * Wrap a git call inside drush.
   *
   * With this shell-alias, you can make a call like 'drush @stage git status', and
   * the command 'git status' will be run on stage.
   *
   * This method can also be used to construct specific git commands by passing
   * a command to the append argument. The idea behind that is you might have
   * developers who don't know git well and need some specific shell-aliases
   * created to hold their hands a little.
   *
   * @param string $append Add more commands.
   * @return string
   *
   */
  public function git($append='') {
    return '!git '. $append;
  }


  /**
   * Returns a formatted !git pull origin dev command.
   *
   * @param string $remote
   *  Set a custom remote.
   * @param string $branch
   *  Set a custom branch
   * @return string
   *  A formatted !git command string.
   *
   */
  public function gitPull($remote='origin', $branch='dev') {
    $append = sprintf('pull %s %s', $remote, $branch);
    return $this->git($append);
  }


  /**
   * Creates a stack of commands to sync one environment to another.
   *
   * @param $source
   *  The environment you're PULLING content FROM.
   * @param $target
   *  The environment you're PUSHING content TO.
   * @param array $options
   *  Optionally define a custom sync stack.
   * @return string
   * @throws \Dalex\Exception
   */
  public function syncEnvironments($source, $target, $options = array()) {
    if (!$options) {
      $options = $this->defaultSyncOptions();
    }
    $cmds = array();
    foreach($options as $option) {
      $cmds[] = $this->preparedCommand($option, $source, $target);
    }
    return '!' . implode($cmds, ' && ');
  }


  /**
   * Returns an array of the default sync options (in the order the should be applied).
   */
  private function defaultSyncOptions() {
    return array(
      DALEX_SQL_DROP,
      DALEX_SQL_SYNC,
      DALEX_RSYNC_FILES,
      DALEX_CC_ALL
    );
  }


  /**
   * Returns a prepared drush command.
   *
   * @param $cmd
   * @param string $source
   * @param string $target
   * @return string
   * @throws \Dalex\Exception
   */
  private function preparedCommand($cmd, $source = '', $target = '') {
    $out = '';
    switch($cmd) {
      case 'sql-drop':
        $out = sprintf('drush sql-drop {{#%s}}', $target);
        break;
      case 'sql-sync':
        $out = sprintf('drush sql-sync {{#%s}} {{#%s}}', $source, $target);
        break;
      case 'rsync-files':
        $out = sprintf('drush rsync {{#%s}}:%%files {{#%s}}:%%files --verbose', $source, $target);
        break;
      case 'cc-all':
        $out = sprintf('drush {{#%s}} cc all', $target);
        break;
    }
    if (!$out) {
      throw new Exception;
    }
    return $out;
  }
}
